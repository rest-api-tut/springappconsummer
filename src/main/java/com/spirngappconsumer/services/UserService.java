package com.spirngappconsumer.services;

import com.spirngappconsumer.User;

import java.util.List;

public interface UserService {
    User showUser(Long id);
    void addUser(User user);
    void removeUser(Long id);
    List<User> getUsers();
}
