package com.spirngappconsumer.services;

import com.spirngappconsumer.User;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.List;

import static com.spirngappconsumer.Config.BASE_URL;

@Service
public class RestTemplateUserService implements UserService{
    private final RestTemplate restTemplate;

    public RestTemplateUserService(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    @Override
    public User showUser(Long id) {
        return restTemplate.getForObject(BASE_URL + "/" + id, User.class);
    }

    @Override
    public void addUser(User user) {
        restTemplate.postForEntity(BASE_URL, user, User.class);
    }

    @Override
    public void removeUser(Long id) {
        restTemplate.delete(BASE_URL + "/" + id);
    }

    @Override
    public List<User> getUsers() {
        return restTemplate.exchange(
                BASE_URL,
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<List<User>>() {}
        ).getBody();
    }
}
